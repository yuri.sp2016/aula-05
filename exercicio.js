const express = require('express');
const app = express();

// Serviço 01.

app.get('/', (req, res) => {
    console.log("Chamado via get");
    res.send("Para logar é necessáro informar usuário e senha");
});

app.get('/usuario', (req, res) => {
    console.log("Chamado via get - consulta usuario");
    res.send("Digite seu usuário");
});

app.get('/senha', (req, res) => {
    console.log("Chamado via get - consulta senha");
    res.send("Digite sua senha");
});

// Serviço 02.

app.post('/nome', (req, res) => {
    console.log("Chamado via post - cadastro de nome");
    res.send("Digite seu nome");
});

app.post('/email', (req, res) => {
    console.log("Chamado via post - cadastro de email");
    res.send("Digite um email valído");
});

app.post('/senha', (req, res) => {
    console.log("Chamado via post - cadastro de nova senha");
    res.send("Digite uma senha");
});

app.post('/idade', (req, res) => {
    console.log("Chamado via post - cadastro de idade");
    res.send("Digite sua idade");
});


app.listen(8000, function () {
    console.log("Projeto iniciado na porta 8000");
});

