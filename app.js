const express = require('express');
const app = express();

app.get('/', (req, res) => {
    console.log("Chamado via get");
    res.send("Method GET - Route: /usuario");
});

app.get('/usuario', (req, res) => {
    console.log("Chamado via get - consulta usuario");
    res.send("Method GET - Route: /usuario");
});


